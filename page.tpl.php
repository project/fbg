<?php
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
	<!--
	*****************************************************************
	Fluid Baseline Grid v1.0.0
	Designed & Built by Josh Hopkins and 40 Horse, http://40horse.com
	Licensed under Unlicense, http://unlicense.org/
	*****************************************************************
	-->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<!-- Optimized mobile viewport -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Place favicon.ico and apple-touch-icon.png in root directory -->
	<?php print $head ?>
    <title><?php print $head_title ?></title>
    <?php print $styles ?>
    <?php print $scripts ?>
    <!--[if lt IE 7]>
      <?php print phptemplate_get_ie_styles(); ?>
    <![endif]-->
</head>

<body<?php print phptemplate_body_class($left, $right); ?>>
	<header>
		<div class="g1">
			<?php
          // Prepare header
          $site_fields = array();
          if ($site_name) {
            $site_fields[] = check_plain($site_name);
          }
          $site_title = implode(' ', $site_fields);
          if ($site_fields) {
            $site_fields[0] = '<span>'. $site_fields[0] .'</span>';
          }
          $site_html = implode(' ', $site_fields);

          if ($logo || $site_title) {
            print '<h1><a href="'. check_url($front_page) .'" title="'. $site_title .'">';
            if ($logo) {
              print '<img src="'. check_url($logo) .'" alt="'. $site_title .'" id="logo" />';
            }
            print $site_html .'</a></h1>';
          }
        ?>
		</div>
		<?php if ($site_slogan): ?>
		<div class="g2">
		  <h2 id="site_slogan"><?php print $site_slogan ?></h2>
		</div>
		<?php endif; ?>
		<?php if ($mission): ?>
		<div id="mission" class="g2">
		  <?php print $mission ?>
		</div>
        <?php endif; ?>
		<?php if ($branding): ?>
		<div class="g2">
		  <?php print $branding ?>
		</div>
		<?php endif; ?>
	</header>
	<div class="cf"></div>
	<div id="content">
		<?php if ($col1): ?>
		<div class="g1">
	      <?php print $col1; ?>
		</div>
		<?php endif; ?>
		<?php if ($col2): ?>
		<div class="g1">
		  <?php print $col2; ?>
		</div>
		<?php endif; ?>
		<?php if ($col3): ?>
		<div class="g1">
		  <?php print $col3; ?>
		</div>
		<?php endif; ?>
		<?php if ($highlighted): ?>
		<div class="g2">
		  <?php print $highlighted; ?>
		</div>
		<?php endif; ?>
		<?php if ($highlighted2): ?>
		<div class="g1">
		  <?php print $highlighted2 ?>
		</div>
		<?php endif; ?>
		<?php if ($content_top): ?>
		<div class="g3">
		  <?php print $content_top ?>
		</div>
		<?php endif; ?>
		<div class="g3">
          <?php print $breadcrumb; ?>
          
          <?php if ($tabs): print '<div id="tabs-wrapper" class="clear-block">'; endif; ?>
          <?php if ($title): print '<h2'. ($tabs ? ' class="with-tabs"' : '') .'>'. $title .'</h2>'; endif; ?>
          <?php if ($tabs): print '<ul class="tabs primary">'. $tabs .'</ul></div>'; endif; ?>
          <?php if ($tabs2): print '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>
          <?php if ($show_messages && $messages): print $messages; endif; ?>
          <?php print $help; ?>
          <div class="clear-block">
            <?php print $content ?>
          </div>
          <?php print $feed_icons ?>		
		</div>
	    <?php if ($content_bottom): ?>
		<div class="g3">
		  <?php print $content_bottom ?>
		</div>
		<?php endif; ?>
	</div>
	<?php if ($footer_message): ?>
	<footer class="g3 cf">
	<?php print $footer_message ?>
	</footer>
    <?php endif; ?>
	<!-- JavaScript at the bottom for fast page loading -->

	<!-- Minimized jQuery from Google CDN -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>

	<!-- HTML5 IE Enabling Script -->
	<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	
	<!-- CSS3 Media Queries -->
	<script src="js/respond.min.js"></script>

	<!-- Optimized Google Analytics. Change UA-XXXXX-X to your site ID -->
	<script>var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];g.src='//www.google-analytics.com/ga.js';s.parentNode.insertBefore(g,s)}(document,'script'))</script>
	
</body>
<?php print $closure ?>
</html>